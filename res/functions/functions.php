<?php
	function renderHeader(array $params = null){	
	/*
	*	De declaratie van het type 'array' is niet per sé nodig.
	*	De '=null' betekent dat als de parameter niet wordt megegeven, $params zal gelijk gesteld worden aan null
	*/

		if (isset($params['title'])) $title = $params['title'];
	
		require('res/templates/header.php');
	}
	
	function renderFooter(array $params = null){
		require('res/templates/footer.php');
	}
	
	//voeg hier andere functies toe
?>