<?php
error_reporting(E_ALL);
ini_set('display_errors','1');

require('res/functions/functions.php');
?>

<?php renderHeader(array('title'=>'Foo')); ?>

<!-- content van pagina -->
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam justo nunc, molestie in scelerisque eget, efficitur nec orci. Quisque laoreet vel lectus ut consectetur. Curabitur viverra vestibulum erat, id consectetur ligula ultricies vitae. Donec vitae quam quam. Morbi finibus tincidunt elit nec sollicitudin. Quisque at pellentesque ipsum. Mauris bibendum odio ut dictum commodo. Vivamus fermentum nisi lectus, id fringilla tellus viverra dapibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi ut suscipit felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
<p>Sed a tortor id mauris tristique volutpat sit amet id augue. Morbi nec sodales augue. Donec molestie vulputate ex ac feugiat. Praesent pellentesque tempor tellus, vitae commodo odio elementum non. Integer pretium cursus libero id interdum. Etiam eu enim eros. Mauris nec augue vel ligula ullamcorper volutpat in sed nunc. Nullam id finibus leo.</p>
<p>Etiam a ante convallis, posuere neque nec, fermentum nisl. Sed id scelerisque ante. Fusce vestibulum nunc erat, ac efficitur mi semper ac. Nulla ultrices sollicitudin lorem, a pharetra eros varius quis. Integer porta leo sed vulputate vulputate. Maecenas gravida quam nisl, et pellentesque felis commodo vel. Suspendisse potenti. In hac habitasse platea dictumst. Fusce at ligula eget diam interdum aliquet. Cras pellentesque turpis congue molestie sollicitudin.</p>
<p>Suspendisse tortor massa, hendrerit non sollicitudin vitae, vestibulum eget dolor. Maecenas finibus ligula vitae facilisis commodo. Nulla scelerisque elit ac sodales iaculis. Maecenas varius tortor elit, et faucibus quam elementum tincidunt. Nunc feugiat arcu dignissim, tincidunt diam in, sollicitudin mauris. Donec ultricies semper mauris, vel lobortis erat feugiat semper. Nullam ut aliquam est, vel finibus sapien. Vestibulum id mollis orci. Etiam quis sem ac augue venenatis dignissim. Vivamus eget urna ipsum. Donec lobortis non lacus ac ornare. Phasellus aliquet vehicula turpis a ultrices. Nam nec magna eu metus faucibus finibus sed a sapien. Nullam ante massa, commodo id lectus non, laoreet egestas nulla. Quisque congue magna eget arcu venenatis interdum. Nam eu massa et leo eleifend convallis a nec elit.</p>
<p>Vivamus metus diam, gravida eu semper sit amet, fermentum feugiat turpis. Praesent eget quam scelerisque, tempus dui ut, tempor erat. Nullam sollicitudin efficitur posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam vel sollicitudin nibh. Donec sodales cursus purus sed rhoncus. Suspendisse potenti. Mauris porttitor hendrerit dolor vel faucibus.</p>

<?php renderFooter(); ?>